<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
    // return view('layouts.reallist_layout');
    // return view('layouts.ace_layout');
});

Route::group(['prefix' => 'admin','namespace'=>'Admin','as' =>'admin.','middleware'=>['web','auth','isAdmin']], function () {
    Route::get('/',function(){
        return redirect()->route('admin.dashboard');
    });
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    // Roles
        // Route::delete('roles/destroy', 'RoleController@massDestroy')->name('roles.massDestroy');
        // Route::resource('roles', 'RoleController');

    // Users
        // Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
        // Route::resource('users', 'UsersController');

});

Route::group(['prefix' => 'editor','namespace'=>'Editor','as' =>'editor.','middleware'=>['web','auth','isEditor']], function () {
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
});

Route::group(['prefix' => 'user','namespace'=>'User','as' =>'user.','middleware'=>['web','auth','isUser']], function () {
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
});
// View::composer('layouts.front_layout', function ($view) {
//     $site_setting = App\Models\SiteSetting::FindOrFail(1);
//     $view->with('site_setting', $site_setting);
// });
// Clear Cache Route

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
