    <!-- include vendor stylesheets used in "DataTables" page. see "/views//pages/partials/table-datatables/@vendor-stylesheets.hbs" -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/pages/table-datatables/@page-style.css">
