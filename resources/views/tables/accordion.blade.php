                {{-- Accordion --}}
                <div class="card ccard bgc-black-tp10 mt-4 mt-md-0 overflow-hidden">
                    <div class="card-body p-0">
                      <div class="accordion" id="accordionExample4">
                        <div class="card border-0 bgc-green-l5">
                          <div class="card-header border-0 bgc-transparent mb-0" id="headingOne4">
                            <h2 class="card-title bgc-transparent text-green-d2 brc-green">
                              <a class="d-style btn btn-white bgc-white btn-brc-tp btn-h-outline-green btn-a-outline-green accordion-toggle border-l-3 radius-0 collapsed" href="#collapseOne4" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne4">
                                1. Anim pariatur cliche

                                <!-- the toggle icon -->
                                <span class="v-collapsed px-3px radius-round d-inline-block brc-grey-l1 border-1 mr-3 text-center position-rc">
                                  <i class="fa fa-angle-down toggle-icon w-2 mx-1px text-90"></i>
                              </span>
                                <span class="v-n-collapsed px-3px radius-round d-inline-block bgc-green mr-3 text-center position-rc">
                                  <i class="fa fa-angle-down toggle-icon w-2 mx-1px text-white text-90 pt-1px"></i>
                              </span>
                              </a>
                            </h2>
                          </div>

                          <div id="collapseOne4" class="collapse" aria-labelledby="headingOne4" data-parent="#accordionExample4">
                            <div class="card-body pt-1 text-dark-m1 border-l-3 brc-green bgc-green-l5">
                              <p>
                                High life accusamus terry richardson ad squid.
                              </p>
                              <p>
                                Moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                              </p>
                              <p>
                                Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                              </p>
                            </div>
                          </div>
                        </div>


                        <div class="card border-0 mt-1px bgc-primary-l5">
                          <div class="card-header border-0 bgc-transparent mb-0" id="headingTwo4">
                            <h2 class="card-title bgc-transparent text-primary-d1 brc-primary-m1">
                              <a class="d-style btn btn-white bgc-white btn-brc-tp btn-h-outline-primary btn-a-outline-primary accordion-toggle border-l-3 radius-0 collapsed" href="#collapseTwo4" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo4">
                                2. Coffee nulla assumenda

                                <!-- the toggle icon -->
                                <span class="v-collapsed px-3px radius-round d-inline-block brc-grey-l1 border-1 mr-3 text-center position-rc">
                                  <i class="fa fa-angle-down toggle-icon w-2 mx-1px text-90"></i>
                              </span>
                                <span class="v-n-collapsed px-3px radius-round d-inline-block bgc-primary mr-3 text-center position-rc">
                                  <i class="fa fa-angle-down toggle-icon w-2 mx-1px text-white text-90 pt-1px"></i>
                              </span>
                              </a>
                            </h2>
                          </div>
                          <div id="collapseTwo4" class="collapse" aria-labelledby="headingTwo4" data-parent="#accordionExample4">
                            <div class="card-body pt-1 text-dark-m1 border-l-3 brc-primary-m1 bgc-primary-l5">
                              <p>
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                              </p>
                              <p>
                                3 wolf moon officia aute, non cupidatat skateboard dolor brunch.
                              </p>
                              <p>
                                Food truck quinoa nesciunt laborum eiusmod.
                              </p>
                            </div>
                          </div>
                        </div>


                        <div class="card border-0 mt-1px bgc-purple-l5">
                          <div class="card-header border-0 bgc-transparent mb-0" id="headingThree4">
                            <h2 class="card-title bgc-transparent text-purple-d2 brc-purple-m1">
                              <a class="d-style btn btn-white bgc-white btn-brc-tp btn-h-outline-purple btn-a-outline-purple accordion-toggle border-l-3 radius-0 collapsed" href="#collapseThree4" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree4">
                                3. Ad vegan excepteur

                                <!-- the toggle icon -->
                                <span class="v-collapsed px-3px radius-round d-inline-block brc-grey-l1 border-1 mr-3 text-center position-rc">
                                  <i class="fa fa-angle-down toggle-icon w-2 mx-1px text-90"></i>
                              </span>
                                <span class="v-n-collapsed px-3px radius-round d-inline-block bgc-purple mr-3 text-center position-rc">
                                  <i class="fa fa-angle-down toggle-icon w-2 mx-1px text-white text-90 pt-1px"></i>
                              </span>
                              </a>
                            </h2>
                          </div>
                          <div id="collapseThree4" class="collapse" aria-labelledby="headingThree4" data-parent="#accordionExample4">
                            <div class="card-body pt-1 text-dark-m1 border-l-3 brc-purple-m1 bgc-purple-l5">
                              <p>
                                Enim eiusmod high life accusamus terry richardson ad squid.
                              </p>
                              <p>
                                Non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                              </p>
                              <p>
                                Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div><!-- /.card -->
                  <br>
