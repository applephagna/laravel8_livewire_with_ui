    <!-- include vendor scripts used in "DataTables" page. see "/views//pages/partials/table-datatables/@vendor-scripts.hbs" -->
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables/media/js/jquery.dataTables.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-colreorder/js/dataTables.colReorder.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-select/js/dataTables.select.js"></script>

    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-buttons/js/dataTables.buttons.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-buttons/js/buttons.html5.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-buttons/js/buttons.print.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-buttons/js/buttons.colVis.js"></script>
    <script src="{{ asset('public/ace_admin') }}/node_modules/datatables.net-responsive/js/dataTables.responsive.js"></script>
    <script src="{{ asset('public/ace_admin') }}/pages/table-datatables/@page-script.js"></script>
