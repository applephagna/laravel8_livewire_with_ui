                {{-- datatable --}}
                <div class="card bcard h-auto">
                    <form autocomplete="off" class="border-t-3 brc-blue-m2">
                        <table id="datatable" class="d-style w-100 table text-dark-m1 text-95 border-y-1 brc-black-tp11 collapsed dtr-table">
                            <!-- add `collapsed` by default ... it will be removed by default -->
                            <!-- thead with .sticky-nav -->
                            <thead class="sticky-nav text-secondary-m1 text-uppercase text-85">
                                <tr>
                                    <th class="td-toggle-details border-0 bgc-white shadow-sm">
                                        <i class="fa fa-angle-double-down ml-2"></i>
                                    </th>

                                    <th class="border-0 bgc-white pl-3 pl-md-4 shadow-sm">
                                        <input type="checkbox" />
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Name
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Office
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Age
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Start date
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Salary
                                    </th>

                                    <th class="border-0 bgc-white shadow-sm w-2">
                                        <!-- the TD will have edit icon -->
                                    </th>
                                </tr>
                            </thead>

                            <tbody class="pos-rel">
                                <tr class="d-style bgc-h-default-l4">
                                    <td class="td-toggle-details pos-rel">
                                        <!-- this empty table cell will show the `+` sign which toggles the hidden cells in responsive (collapsed) mode -->
                                        <div class="position-lc h-95 ml-1px border-l-3 brc-purple-m1">
                                            <!-- this decorative highlight border will be shown only when table is collapsed (responsive) -->
                                        </div>
                                    </td>
                                    <td class="pl-3 pl-md-4 align-middle pos-rel">
                                        <input type="checkbox" />
                                        <div class="d-n-collapsed position-lc h-95 ml-1px border-l-3 brc-purple-m1">
                                            <!-- this decorative highlight border will be shown only when table is in full mode (not collapsed >> .d-n-collapsed) -->
                                        </div>
                                    </td>
                                    <td>
                                        <span class="text-105">
                                            Tigerd Nixon
                                        </span>
                                        <div class="text-95 text-secondary-d1">
                                            System Architect
                                        </div>
                                    </td>

                                    <td class="text-grey">
                                        Edinburgh
                                        <div><span class='badge bgc-orange-d1 text-white badge-sm'>On Vacation</span></div>
                                    </td>

                                    <td class="text-600 text-grey-d1">
                                        61
                                    </td>

                                    <td class="text-grey">
                                        2011/04/25
                                    </td>

                                    <td>
                                        <i class="fa fa-arrow-down text-orange-d1"></i> $320,800
                                    </td>

                                    <td class="align-middle">
                                        <span class="d-none d-lg-inline">
                                            <a data-rel="tooltip" data-action="edit" title="Edit" href="#" class="v-hover">
                                                <i class="fa fa-edit text-blue-m1 text-120"></i>
                                            </a>
                                        </span>

                                        <span class="d-lg-none text-nowrap">
                                            <a title="Edit" href="#" class="btn btn-outline-info shadow-sm px-4 btn-bgc-white">
                                                <i class="fa fa-pencil-alt mx-1"></i>
                                                <span class="ml-1 d-md-none">Edit</span>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                <tr class="d-style bgc-h-default-l4">
                                    <td class="td-toggle-details pos-rel">
                                        <!-- this empty table cell will show the `+` sign which toggles the hidden cells in responsive (collapsed) mode -->
                                    </td>
                                    <td class="pl-3 pl-md-4 align-middle pos-rel">
                                        <input type="checkbox" />
                                    </td>
                                    <td>
                                        <span class="text-105">
                                            Garrett Winters
                                        </span>
                                        <div class="text-95 text-secondary-d1">
                                            Accountant
                                        </div>
                                    </td>
                                    <td class="text-grey">
                                        Tokyo
                                        <div><span class='badge badge-primary badge-sm'>2011/04/25</span></div>
                                    </td>
                                    <td class="text-600 text-grey-d1">
                                        63
                                    </td>
                                    <td class="text-grey">
                                        2011/07/25
                                    </td>
                                    <td>
                                        <i class="fa fa-arrow-down text-orange-d1"></i> $170,750
                                    </td>
                                    <td class="align-middle">
                                        <span class="d-none d-lg-inline">
                                            <a data-rel="tooltip" data-action="edit" title="Edit" href="#" class="v-hover">
                                                <i class="fa fa-edit text-blue-m1 text-120"></i>
                                            </a>
                                        </span>

                                        <span class="d-lg-none text-nowrap">
                                            <a title="Edit" href="#" class="btn btn-outline-info shadow-sm px-4 btn-bgc-white">
                                                <i class="fa fa-pencil-alt mx-1"></i>
                                                <span class="ml-1 d-md-none">Edit</span>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                <tr class="d-style bgc-h-default-l4">
                                    <td class="td-toggle-details pos-rel">
                                        <!-- this empty table cell will show the `+` sign which toggles the hidden cells in responsive (collapsed) mode -->
                                    </td>
                                    <td class="pl-3 pl-md-4 align-middle pos-rel">
                                        <input type="checkbox" />
                                    </td>
                                    <td>
                                        <span class="text-105">
                                            Ashton Cox
                                        </span>
                                        <div class="text-95 text-secondary-d1">
                                                Junior Technical Author
                                        </div>
                                    </td>
                                    <td class="text-grey">
                                        San Francisco
                                    </td>
                                    <td class="text-600 text-grey-d1">
                                        66
                                    </td>
                                    <td class="text-grey">
                                        2009/01/12
                                    </td>
                                    <td>
                                        <i class="fa fa-arrow-up text-blue-d1"></i> $86,000
                                    </td>
                                    <td class="align-middle">
                                        <span class="d-none d-lg-inline">
                                            <a data-rel="tooltip" data-action="edit" title="Edit" href="#" class="v-hover">
                                                <i class="fa fa-edit text-blue-m1 text-120"></i>
                                            </a>
                                        </span>
                                        <span class="d-lg-none text-nowrap">
                                            <a title="Edit" href="#" class="btn btn-outline-info shadow-sm px-4 btn-bgc-white">
                                                <i class="fa fa-pencil-alt mx-1"></i>
                                                <span class="ml-1 d-md-none">Edit</span>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>                {{-- datatable --}}
                <div class="card bcard h-auto">
                    <form autocomplete="off" class="border-t-3 brc-blue-m2">
                        <table id="datatable" class="d-style w-100 table text-dark-m1 text-95 border-y-1 brc-black-tp11 collapsed dtr-table">
                            <!-- add `collapsed` by default ... it will be removed by default -->
                            <!-- thead with .sticky-nav -->
                            <thead class="sticky-nav text-secondary-m1 text-uppercase text-85">
                                <tr>
                                    <th class="td-toggle-details border-0 bgc-white shadow-sm">
                                        <i class="fa fa-angle-double-down ml-2"></i>
                                    </th>

                                    <th class="border-0 bgc-white pl-3 pl-md-4 shadow-sm">
                                        <input type="checkbox" />
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Name
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Office
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Age
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Start date
                                    </th>

                                    <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                                        Salary
                                    </th>

                                    <th class="border-0 bgc-white shadow-sm w-2">
                                        <!-- the TD will have edit icon -->
                                    </th>
                                </tr>
                            </thead>

                            <tbody class="pos-rel">
                                <tr class="d-style bgc-h-default-l4">
                                    <td class="td-toggle-details pos-rel">
                                        <!-- this empty table cell will show the `+` sign which toggles the hidden cells in responsive (collapsed) mode -->
                                        <div class="position-lc h-95 ml-1px border-l-3 brc-purple-m1">
                                            <!-- this decorative highlight border will be shown only when table is collapsed (responsive) -->
                                        </div>
                                    </td>
                                    <td class="pl-3 pl-md-4 align-middle pos-rel">
                                        <input type="checkbox" />
                                        <div class="d-n-collapsed position-lc h-95 ml-1px border-l-3 brc-purple-m1">
                                            <!-- this decorative highlight border will be shown only when table is in full mode (not collapsed >> .d-n-collapsed) -->
                                        </div>
                                    </td>
                                    <td>
                                        <span class="text-105">
                                            Tigerd Nixon
                                        </span>
                                        <div class="text-95 text-secondary-d1">
                                            System Architect
                                        </div>
                                    </td>

                                    <td class="text-grey">
                                        Edinburgh
                                        <div><span class='badge bgc-orange-d1 text-white badge-sm'>On Vacation</span></div>
                                    </td>

                                    <td class="text-600 text-grey-d1">
                                        61
                                    </td>

                                    <td class="text-grey">
                                        2011/04/25
                                    </td>

                                    <td>
                                        <i class="fa fa-arrow-down text-orange-d1"></i> $320,800
                                    </td>

                                    <td class="align-middle">
                                        <span class="d-none d-lg-inline">
                                            <a data-rel="tooltip" data-action="edit" title="Edit" href="#" class="v-hover">
                                                <i class="fa fa-edit text-blue-m1 text-120"></i>
                                            </a>
                                        </span>

                                        <span class="d-lg-none text-nowrap">
                                            <a title="Edit" href="#" class="btn btn-outline-info shadow-sm px-4 btn-bgc-white">
                                                <i class="fa fa-pencil-alt mx-1"></i>
                                                <span class="ml-1 d-md-none">Edit</span>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                <tr class="d-style bgc-h-default-l4">
                                    <td class="td-toggle-details pos-rel">
                                        <!-- this empty table cell will show the `+` sign which toggles the hidden cells in responsive (collapsed) mode -->
                                    </td>
                                    <td class="pl-3 pl-md-4 align-middle pos-rel">
                                        <input type="checkbox" />
                                    </td>
                                    <td>
                                        <span class="text-105">
                                            Garrett Winters
                                        </span>
                                        <div class="text-95 text-secondary-d1">
                                            Accountant
                                        </div>
                                    </td>
                                    <td class="text-grey">
                                        Tokyo
                                        <div><span class='badge badge-primary badge-sm'>2011/04/25</span></div>
                                    </td>
                                    <td class="text-600 text-grey-d1">
                                        63
                                    </td>
                                    <td class="text-grey">
                                        2011/07/25
                                    </td>
                                    <td>
                                        <i class="fa fa-arrow-down text-orange-d1"></i> $170,750
                                    </td>
                                    <td class="align-middle">
                                        <span class="d-none d-lg-inline">
                                            <a data-rel="tooltip" data-action="edit" title="Edit" href="#" class="v-hover">
                                                <i class="fa fa-edit text-blue-m1 text-120"></i>
                                            </a>
                                        </span>

                                        <span class="d-lg-none text-nowrap">
                                            <a title="Edit" href="#" class="btn btn-outline-info shadow-sm px-4 btn-bgc-white">
                                                <i class="fa fa-pencil-alt mx-1"></i>
                                                <span class="ml-1 d-md-none">Edit</span>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                <tr class="d-style bgc-h-default-l4">
                                    <td class="td-toggle-details pos-rel">
                                        <!-- this empty table cell will show the `+` sign which toggles the hidden cells in responsive (collapsed) mode -->
                                    </td>
                                    <td class="pl-3 pl-md-4 align-middle pos-rel">
                                        <input type="checkbox" />
                                    </td>
                                    <td>
                                        <span class="text-105">
                                            Ashton Cox
                                        </span>
                                        <div class="text-95 text-secondary-d1">
                                                Junior Technical Author
                                        </div>
                                    </td>
                                    <td class="text-grey">
                                        San Francisco
                                    </td>
                                    <td class="text-600 text-grey-d1">
                                        66
                                    </td>
                                    <td class="text-grey">
                                        2009/01/12
                                    </td>
                                    <td>
                                        <i class="fa fa-arrow-up text-blue-d1"></i> $86,000
                                    </td>
                                    <td class="align-middle">
                                        <span class="d-none d-lg-inline">
                                            <a data-rel="tooltip" data-action="edit" title="Edit" href="#" class="v-hover">
                                                <i class="fa fa-edit text-blue-m1 text-120"></i>
                                            </a>
                                        </span>
                                        <span class="d-lg-none text-nowrap">
                                            <a title="Edit" href="#" class="btn btn-outline-info shadow-sm px-4 btn-bgc-white">
                                                <i class="fa fa-pencil-alt mx-1"></i>
                                                <span class="ml-1 d-md-none">Edit</span>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
