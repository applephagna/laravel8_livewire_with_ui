@include('layouts.partials.admin.ace_head')

  <body>
    <div class="body-container">
        {{-- top navbar --}}
        @include('layouts.partials.admin.top_nav')

      <div class="main-container bgc-white">
        @include('layouts.partials.admin.left_sidebar')

        <div role="main" class="main-content">
          <div class="page-content container container-plus">
            <div class="row mt-3">
              <div class="col-md-12">
                {{-- for content --}}
                @yield('content')

              </div>
            </div>

            @include('layouts.partials.admin.footer')
          </div>
        </div>
      </div>
    </div>
    <!-- include common vendor scripts used in demo pages -->
    @include('layouts.partials.admin.script')
    @stack('scripts')
  </body>

</html>
