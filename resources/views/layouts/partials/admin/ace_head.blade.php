<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <base href="/" />

    <title>@yield('pagetitle','ACE Admin Dashboard Layout')</title>

    <!-- include common vendor stylesheets & fontawesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/node_modules/@fortawesome/fontawesome-free/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/node_modules/@fortawesome/fontawesome-free/css/regular.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/node_modules/@fortawesome/fontawesome-free/css/brands.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/node_modules/@fortawesome/fontawesome-free/css/solid.css">
    <!-- include fonts -->
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/dist/css/ace-font.css"> --}}
    <!-- ace.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/ace_admin') }}/dist/css/ace.css">
    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{ asset('public/ace_admin') }}/favicon.png" />
    <style>
        body{
            font-family: 'Hanuman', 'serif' !important;
        }
    </style>
    @stack('styles')
  </head>
