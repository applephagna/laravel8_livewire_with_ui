<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsUserMiddeware
{
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check() && Auth::user()->role->name==='User'){
            return $next($request);
        } else {
            return redirect()->route('login');
        }
    }
}
