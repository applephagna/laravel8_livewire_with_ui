<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        Role::create([
            'name' => 'Admin',
            'display_name' => 'Administrator',
            'description' => 'Administrator can manage all action in project',
            'slug' => Str::slug('admin'),
            'guard_name' => 'web',
            'status' => 1,
        ]);
        Role::create([
            'name' => 'Editor',
            'display_name' => 'Editor',
            'description' => 'Editor can manage some action in project',
            'slug' => Str::slug('editor'),
            'guard_name' => 'web',
            'status' => 1,
        ]);
        Role::create([
            'name' => 'User',
            'display_name' => 'User',
            'description' => 'User can view infomation in project',
            'slug' => Str::slug('User'),
            'guard_name' => 'web',
            'status' => 1,
        ]);
    }
}
