<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'role_id' => 1,
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('Phagna@sa225'),
            'utype' => 'ADM',
        ]);
        User::create([
            'role_id' => 2,
            'name' => 'Editor',
            'email' => 'editor@gmail.com',
            'password' => bcrypt('Phagna@sa225'),
            'utype' => 'EDT',
        ]);
        User::create([
            'role_id' => 3,
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' => bcrypt('Phagna@sa225'),
            'utype' => 'USR',
        ]);
    }
}
