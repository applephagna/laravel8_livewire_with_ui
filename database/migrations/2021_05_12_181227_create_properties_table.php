<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->decimal('regular_price');
            $table->decimal('sale_price')->nullable();
            $table->string('PID')->nullable()->comment('Properties ID or Code');
            $table->enum('property_status',['Booking','Sold Out','On Sale','Offer']);

            $table->boolean('featured')->default(false);

            $table->string('image')->nullable();
            $table->text('images')->nullable();
            $table->text('size')->nullable()->comment('Size of Properties sqrt');
            $table->text('address')->nullable()->comment('location or address of properties');
            $table->text('landmarks')->nullable()->comment('Near Landmarks');
            // $table->text('location')->nullable()->comment('Provinces');


            $table->timestamps();
            $table->unsignedBigInteger('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('property_type_id')->references('id')->on('property_types')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('location_id')->references('id')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
